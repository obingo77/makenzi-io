 #syntax=docker/dockerfile:1

FROM golang:1.16-alpine
LABEL      maintainer="Obingo77 <@obingo76>"
WORKDIR /app



COPY go.mod ./

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download
COPY *.go ./

EXPOSE 8080

# Build the Go app
RUN go build main .


CMD ["./main"]